# Historia

- Entrega  via Pull Request día 16/nov


- Crea un controlador LoginController (ya existe)
    - Método index: muestra la vista de login
    - Método login: comprueba los datos del formulario de login.
        - Si son válidos, guarda el usuario en sesión.
        - Si no son válidos reenvía al usuario a la ruta anterior con un mensaje de error (usa $_SESSION['error'])
        - A partir de ahí debes mostrar en la cabecera la información de usuario (header.php)
        - Si el usuario existe: nombre + link de logout
        - Si no existe link a login.
    - El método logout debe cerrar la sesión y reenviar a login.
