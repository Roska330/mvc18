USE `mvc18trabajo`;


DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(50) DEFAULT NULL,
  `price` double not null,
  `type_id` int,
  INDEX (type_id),
 FOREIGN KEY (type_id) REFERENCES product_types(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `products` VALUES
(1, 'ambar', 0.8, 1),
(2, 'Coca Cola', 1, 2),
(3, 'Fanta Lim�n', 1, 2),
(4, 'Fanta Naranja', 1, 2),
(5, 'Gin Tonic', 3, 5)
;


