# HISTORIA 1:

- Entrega  via Pull Request día 14/nov

- El sistema debe permitir mantener una lista de usuarios:
```sql

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(50) DEFAULT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `active` BOOLEAN DEFAULT false,
  `admin` BOOLEAN DEFAULT false,
  UNIQUE INDEX (`email`)
)

```

- Ejecuta el fichero users.sql desde phpmyadmin. Crearás la BBDD mvc18trabajo con una tabla users y 7 registros iniciales.


Se espera acceder a las siguientes rutas:

 - `/user` y `/user/index` Lista de usuarios

 - `/user/show/{id}` Detalles del usuario con id {id}. La contraseña ni se modifica ni se muestra.

 - `/register`  y `/register/index` Formulario de registro (alta de usuario).
 - `/register/register` Tomar los datos del registro



//pendiente para más adelante: login, logout, renovar contraseña, perfil de usuario, borrar usuario, ....






