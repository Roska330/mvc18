# Historia 5

- Entrega  via Pull Request día 23/nov

- Vamos a gestionar la realización de "pedidos" en la peña. Se trata de que cada usuario registre aquellos productos que consume.
- Añade las siguientes rutas:
    /basket/add/{id}  Este enlace debe estar en la lista de productos. Al hacer click sobre el se añade a la lista de productos en la cesta.
    Si un producto se clica y ya existe en la cesta se aumenta la cantidad.

- /basket Muestra el contenido de la cesta en cada momento (producto, cantidad, precio).
- /basket/remove/{id}
- /basket/up/{id} y /basket/down/{id} aumenta o disminuye en 1 la cantidad de cada producto. (enlaces en la vista de /basket).

- Al cerrar sesión se debe perder el contenido de la cesta. El próximo día veremos como guardarla en BBDD.
