<?php
namespace Core;
/**
*
*/
use PDO;
class Model
{

    function __construct()
    {

    }
    protected final static function db(){
        $dsn = "mysql:dbname=mvc18trabajo;host=127.0.0.1";
        $usuario = "root";
        $contraseña = "root";
        try {
            $db = new PDO($dsn,$usuario,$contraseña);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(\PDOException $e) {
            echo "Fallo la conexion: ";
            echo "<pre>" . $e.getMessage();
        }
        return $db;
    }
}
