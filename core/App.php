<?php
namespace Core;

class App
{

    function __construct()
    {
        require_once "../app/models/User.php";
        session_start();

        $url = isset($_GET["url"]) ? $_REQUEST["url"] : "home";
        echo "<br>";

        $arguments = explode("/", trim($url,"/"));
        $controller = array_shift($arguments);
        $controller = ucwords($controller) . "Controller";

        $method = count($arguments) > 0 ? array_shift($arguments) : "index";


        $file = "../app/controllers/$controller" . ".php";
        if(file_exists($file))
            require_once $file;
        else{

            header("HTTP/1.0 404 Not Found");
            echo "<br>No encontrado";
            die;
        }

        $controller = "\\App\\Controllers\\" . $controller;
        $controllerObject = new $controller;
        if(method_exists($controller,$method)){
            try{
                $controllerObject->$method($arguments);
            } catch (\Exception $e) {
                header("HTTP/1.0 500 Internal Error");
                echo $e->getMessage();
                echo "<pre>";
                echo $e->getTraceAsString();
            }
        }
        else {
            header("HTTP/1.0 404 Not Found");
            echo "<br>No encontrado";
            die;

        }
    }
}
