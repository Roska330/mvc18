<!DOCTYPE html>
<html lang="es">
<head>
<?php require "../app/views/parts/head.php"; ?>
<?php require "../app/views/parts/header.php"; ?>
</head>
<body>
    <main role="main" class="container">
    <br>
    <div class="starter-template">
        <h1>Home</h1>
        <p class="lead">Página de inicio</p>
    </div>
    </main>
<!-- Cargamos el footer -->
<?php require "../app/views/parts/footer.php"; ?>

</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
