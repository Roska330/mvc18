<nav class="navbar navbar-expand-md navbar-dark bg-dark">
  <a class="navbar-brand" href="#">La PeÑa</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="
  #navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/home"> Home </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/user"> Peñistas </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/product"> Productos </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/productType"> Tipo Productos </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/order"> Pedidos </a>
      </li>
    </ul>
    <ul class="navbar-nav">
      </li>
      <li class="nav-item">
      <a class="nav-link" href="/order/basket"> Cesta </a>
    </li>
      <li class="nav-item active">
        <a class="nav-link" href="/login/log"><?php echo isset($_SESSION["user"]) && !empty($_SESSION["user"]) ? $_SESSION["user"]->name . " - Logout" : "Login" ?></a>
      </li>
    </ul>
  </div>
</nav>
