<?php require "../app/views/parts/head.php"; ?>
<body>
    <?php require "../app/views/parts/header.php"; ?>
    <main role="main" class="container">
        <br>
        <div class="starter-template">
            <h1>Lista de tipos de productos</h1>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nombre</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($types as $type): ?>
                        <tr>
                            <td>
                                <?php echo $type->name ?>
                            </td>
                            <td>
                            <a class="btn btn-primary" href="/product/productsOfCategory/<?php echo $type->id ?>">Ver</a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
        </table>
            <hr>

        </div>
    </main>
    <?php require "../app/views/parts/footer.php"; ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
