<!DOCTYPE html>
<html>
<?php require "../app/views/parts/head.php" ?>
<body>
    <?php require "../app/views/parts/header.php" ?>
    <main role="main" class="container">
      <br>
      <div class="starter-template">
        <h1>Productos con categoría: <b><?php echo $type->name ?></b> </h1>
        <table class="table table-striped">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Precio</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($products as $p): ?>
                <tr>
                  <td><?php echo $p->name ?></td>
                  <td><?php echo $p->price . "€" ?></td>
              </tr>
          <?php endforeach ?>
      </tbody>
  </table>
</div>
<a href="/productType">Volver</a>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
