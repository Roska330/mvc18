<!doctype html>
<html lang="es">
<?php require "../app/views/parts/head.php" ?>
<body>
  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <br>
    <div class="starter-template">
      <h1 style="color: red"><?php echo isset($_SESSION["message"]) ? $_SESSION["message"] : ""  ?></h1>
      <h1>Cesta</h1>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Cantidad</th>
            <th>Precio</th>
          </tr>
        </thead>
        <tbody>
          <?php if (isset($_SESSION["basket"])): ?>
            <?php foreach ($_SESSION["basket"] as $product): ?>
            <tr>
              <td><?php echo $product->name ?></td>
              <td>
                <a href="/order/remove/<?php echo $product->id ?>" class="btn btn-primary"> - </a>
                <?php echo $product->quantity ?>
                <a href="/order/add/<?php echo $product->id ?>" class="btn btn-primary"> + </a>
              </td>
              <td><?php echo $product->price . "€" ?></td>
           </tr>
         <?php endforeach ?>
          <?php endif ?>

       </tbody>
     </table>
     <hr>
  </div>
  <a href="/register/storeOrders">Realizar pedido</a>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
