<!DOCTYPE html>
<html>
<?php require "../app/views/parts/head.php" ?>
<body>
    <?php require "../app/views/parts/header.php" ?>

    <main role="main" class="container">
    <br>
    <div class="starter-template">
        <h1>Detalle de pedido</h1>
        <ul>
            <?php foreach ($products as $product): ?>
            <li><?php echo "Producto: " . $product->product()->name ?></li>
            <li><?php echo "Precio: " . ($product->price * $product->quantity) . " €" ?></li>
            <li><?php echo "Cantidad: " . $product->quantity ?></li>
            ----------------------------------------------------------------------------
            <?php endforeach ?>

        </ul>
    </div>
    <a href="/order">Volver</a>
    </main>

    <?php require "../app/views/parts/footer.php" ?>

</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
