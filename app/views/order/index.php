<?php require "../app/views/parts/head.php"; ?>
<body>
    <?php require "../app/views/parts/header.php"; ?>
    <main role="main" class="container">
    <br>
    <div class="starter-template">
        <h1>Lista de pedidos</h1>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Precio</th>
                    <th>Usuario</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($orders as $order): ?>
                <tr>
                    <td><?php echo $order->date ?></td>
                    <td><?php echo $order->price ?></td>
                    <td><?php echo $order->user()->name ?></td>

                    <td>
                        <a class="btn btn-primary" href="/order/show/<?php echo $order->id ?>">Ver</a>
                    </td>
                </tr>
                <?php endforeach ?>

            </tbody>
        </table>


        <hr>
    </div>

    </main>
<!-- Cargamos el footer -->
<?php require "../app/views/parts/footer.php"; ?>

</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
