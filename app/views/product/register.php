<!doctype html>
<html lang="es">
<head>
    <?php require "../app/views/parts/head.php"  ?>
    <title>Nuevo producto</title>
</head>
<body>
    <?php require "../app/views/parts/header.php" ?>


    <main role="main" class="container">
      <div class="starter-template">
        <h1 style="color: red"><?php echo isset($_SESSION["message_product"]) ? $_SESSION["message_product"] : ""  ?></h1>
        <h1>Nuevo producto</h1>

        <form action="/register/storeProduct" method="post">

          <div class="form-group">
            <label>Nombre:</label>
            <input type="text" class="form-control" name="name" placeholder="Introduzca el nombre...">
        </div>

        <div class="form-group">
            <label">Precio:</label>
            <input type="number" class="form-control" name="price" placeholder="Introduzca el precio...">
        </div>

        <div class="form-group">
            <label>Tipo producto:</label>
            <select name="type" class="form-control">
                <?php foreach ($types as $type): ?>
                    <option value="<?php echo $type->id ?>"><?php echo $type->name ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <button type="submit" class="btn btn-default">Agregar producto</button>

    </form>

</div>
<a href="/product">Volver</a>
</main>


</body>
</html>
