<!doctype html>
<html lang="es">
<head>
    <?php require "../app/views/parts/head.php"  ?>
    <title>Modificar Producto</title>
</head>
<body>
    <?php require "../app/views/parts/header.php" ?>
    <main role="main" class="container">
      <div class="starter-template">
        <h1>Modifica el producto:  <?php echo $product->name ?></h1>
        <form action="/product/update" method="post">
        <input type="hidden" name="id" value="<?php echo $product->id ?>">
        <div class="form-group">
            <label>Nombre:</label>
            <input type="text" class="form-control" name="name" value="<?php echo $product->name ?>">
          </div>

        <div class="form-group">
            <label>Precio:</label>
            <input type="text" class="form-control" name="price" value="<?php echo $product->price ?>">
        </div>

        <div class="form-group">
            <label>Tipo producto:</label>
            <select name="type" class="form-control">
                <?php foreach ($types as $type): ?>
                    <option value="<?php echo $type->id ?>" <?php echo $type->id == $product->type_id ?'selected' : ''?>><?php echo $type->name ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <button type="submit" class="btn btn-default">Modificar</button>
        |
        <a class="btn" href="/product">Cancelar</a>
    </form>
</div>
</main>


</body>
</html>
