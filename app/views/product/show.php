<!DOCTYPE html>
<html>
<?php require "../app/views/parts/head.php" ?>
<body>
    <?php require "../app/views/parts/header.php" ?>

    <main role="main" class="container">
    <br>
    <div class="starter-template">
        <h1>Detalle de producto</h1>
        <ul>
            <li><?php echo "Nombre: " . $product->name ?></li>
            <li><?php echo "Precio: " . $product->price . " €" ?></li>
            <li><?php echo "Tipo producto: " . $product->type_id ?></li>

        </ul>
    </div>
    <a href="/product">Volver</a>
    </main>

    <?php require "../app/views/parts/footer.php" ?>

</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
