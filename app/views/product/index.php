<?php require "../app/views/parts/head.php"; ?>
<body>
    <?php require "../app/views/parts/header.php"; ?>
    <main role="main" class="container">
    <br>
    <div class="starter-template">
        <h1 style="color: red"><?php echo isset($_SESSION["message"]) ? $_SESSION["message"] : ""  ?></h1>
        <h1>Lista de productos</h1>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Precio</th>
                    <th>Tipo producto</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $product): ?>
                <tr>
                    <td><?php echo $product->name ?></td>
                    <td><?php echo $product->price . " €" ?></td>
                    <td><?php echo $product->type()->name ?></td>

                    <td>
                        <a class="btn btn-primary" href="/order/add/<?php echo $product->id ?>">Comprar</a>
                         |
                        <a class="btn btn-primary" href="/product/show/<?php echo $product->id ?>">Ver</a>
                         |
                        <a class="btn btn-primary" href="/product/delete/<?php echo $product->id ?>">Borrar</a>
                        |
                        <a class="btn btn-primary" href="/product/edit/<?php echo $product->id ?>">Modificar</a>
                    </td>
                </tr>
                <?php endforeach ?>

            </tbody>
        </table>

        <hr>

        <a href="/product/create">Nuevo producto</a>
    </div>

    </main>
<!-- Cargamos el footer -->
<?php require "../app/views/parts/footer.php"; ?>

</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
