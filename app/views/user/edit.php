<!doctype html>
<html lang="es">
<head>
    <?php require "../app/views/parts/head.php"  ?>
    <title>Modificar Peñista</title>
</head>
<body>
    <?php require "../app/views/parts/header.php" ?>


    <main role="main" class="container">
      <div class="starter-template">
        <h1>Modifica el peñista:  <?php echo $user->name ?></h1>

        <form action="/user/update" method="post">
        <input type="hidden" name="id" value="<?php echo $user->id ?>">
        <div class="form-group">
            <label for="email">Nombre:</label>
            <input type="text" class="form-control" name="name" value="<?php echo $user->name ?>">
          </div>

        <div class="form-group">
            <label for="email">Apellidos:</label>
            <input type="text" class="form-control" name="surname" value="<?php echo $user->surname ?>">
        </div>

        <div class="form-group">
            <label for="email">Fecha de nacimiento:</label>
            <input type="text" class="form-control" name="birthdate" value="<?php echo $user->date->format('d/m/Y') ?>" min="1990-01-01T00:00" max="2018-11-13T22:30">
        </div>

        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" name="email" value="<?php echo $user->email ?>">
        </div>

        <button type="submit" class="btn btn-default">Modificar</button>
        |
        <a class="btn" href="/user">Cancelar</a>
    </form>

</div>

</main>


</body>
</html>
