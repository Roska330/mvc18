<!doctype html>
<html lang="es">
<head>
    <?php require "../app/views/parts/head.php"  ?>
    <title>Nuevo peñista</title>
</head>
<body>
    <?php require "../app/views/parts/header.php" ?>


    <main role="main" class="container">
      <div class="starter-template">
        <h1>Alta de Peñista</h1>

        <form action="/register/storeUser" method="post">

          <div class="form-group">
            <label>Nombre:</label>
            <input type="text" class="form-control" name="name" placeholder="Introduzca su nombre...">
        </div>

        <div class="form-group">
            <label">Apellidos:</label>
            <input type="text" class="form-control" name="surname" placeholder="Introduzca sus apellidos...">
        </div>

        <div class="form-group">
            <label>Email:</label>
            <input type="email" class="form-control" name="email" placeholder="Introduzca su correo...">
        </div>

        <div class="form-group">
            <label>Fecha de nacimiento:</label>
            <input type="datetime-local" class="form-control" name="birthdate" placeholder="2018-11-13 22:30" min="1990-01-01T00:00" max="2018-11-13T22:30">
        </div>

        <div class="form-group">
            <label>Contraseña:</label>
            <input type="password" class="form-control" name="password" placeholder="Introduzca su contraseña...">
        </div>

        <button type="submit" class="btn btn-default">Agregar peñista</button>

    </form>

</div>
</main>
</body>
</html>
