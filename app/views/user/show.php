<!DOCTYPE html>
<html>
<?php require "../app/views/parts/head.php"; ?>
<body>
    <?php require "../app/views/parts/header.php"; ?>

    <main role="main" class="container">
    <br>
    <div class="starter-template">
        <h1>Detalle de peñista</h1>
        <ul>
            <li><?php echo "Nombre: " . $user->name ?></li>
            <li><?php echo "Apellidos: " . $user->surname  ?></li>
            <li><?php echo "Email: " . $user->email ?></li>
            <li><?php echo "Fecha de nacimiento: " . $user->date->format("d/m/Y") ?></li>

        </ul>
    </div>
    <a href="/user">Volver</a>
    </main>


    <?php require "../app/views/parts/footer.php"; ?>

</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
