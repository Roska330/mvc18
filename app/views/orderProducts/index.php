<?php require "../app/views/parts/head.php"; ?>
<body>
    <?php require "../app/views/parts/header.php"; ?>
    <main role="main" class="container">
    <br>
    <div class="starter-template">
        <h1>Lista de pedidos</h1>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Precio</th>
                    <th>Usuario</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($orders as $order): ?>
                <tr>
                    <td><?php echo $order->datee ?></td>
                    <td><?php echo $order->price ?></td>
                    <td><?php echo $order->user_id ?></td>

                </tr>
                <?php endforeach ?>

            </tbody>
        </table>


        <hr>
    </div>
    <a class="btn" href="/order">Volver</a>
    </main>
<!-- Cargamos el footer -->
<?php require "../app/views/parts/footer.php"; ?>

</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
