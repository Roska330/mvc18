<?php
namespace App\Controllers;
use \App\Models\ProductType;

class ProductTypeController
{

    function __construct()
    {

    }


    public function index()
    {

        if(isset($_SESSION["user"]) && !empty(isset($_SESSION["user"]))) {
            //Se ha logeado ya el usuario
            $types = ProductType::all();
            require "../app/views/productType/index.php";

        } else {
            //No se ha logueado el usuario
            header("Location:/login");

        }
    }




}

