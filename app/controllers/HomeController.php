<?php
namespace App\Controllers;

class HomeController
{

    function __construct()
    {

    }

    public function index()
    {
        if(isset($_SESSION["user"]) && !empty(isset($_SESSION["user"]))) {
            //Se ha logeado ya el usuario
            header("Location:/user");
        } else {
            //No se ha logueado el usuario
            header("Location:/login");

        }
    }
}
