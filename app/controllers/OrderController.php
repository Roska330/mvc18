<?php
namespace App\Controllers;
use \App\Models\Order;
use \App\Models\Product;
use \App\Models\Basket;

class OrderController
{

    function __construct(){}

    public function index()
    {
        $orders = Order::all();
        require "../app/views/order/index.php";
    }

    public function basket()
    {
        // unset($_SESSION["basket"]);
        // exit();
        if(isset($_SESSION["user"]) && !empty(isset($_SESSION["user"]))) {
            //Se ha logeado ya el usuario
            if(isset($_SESSION["basket"])){
                $basket = $_SESSION["basket"];
            }else{
                $basket = [];
            }
            require "../app/views/order/basket.php";

        } else {
            //No se ha logueado el usuario
            header("Location:/login");
        }
    }

    public function add($args)
    {
        $basket = [];
        $id = (int)$args[0];
        $product = Product::find($id);

        if(isset($_SESSION["basket"])) {
            $basket = $_SESSION["basket"];
        }

        if(!array_key_exists($id,$basket)) {
            $prod = new Basket();
            $prod->id = $product->id;
            $prod->name = $product->name;
            $prod->quantity = 1;
            $prod->price = $product->price;
        } else {
            $prod = $basket[$id];
            $prod->id = $product->id;
            $prod->name = $product->name;
            $prod->quantity += 1;
            $prod->price += $product->price;
        }
        $basket[$id] = $prod;
        $_SESSION["basket"] = $basket;
        require "../app/views/order/basket.php";

    }

    public function remove($args)
    {
        $basket = [];
        $id = (integer)$args[0];
        $product = Product::find($id);
        if(isset($_SESSION["basket"])) {
            $basket = $_SESSION["basket"];
            $prod = $basket[$id];
            if((int)$prod->quantity > 1) {
                $prod->quantity -= 1;
                $prod->price -= $product->price;
            } else {
                unset($basket[$id]);
            }
            $_SESSION["basket"] = $basket;
            require "../app/views/order/basket.php";


        } else {
            header("Location:/product");

        }
    }

    public function show($args)
    {
        $id = (int) $args[0];
        $products = Order::products($id);

        require "../app/views/order/show.php";
    }
}
