<?php
namespace App\Controllers;
use \App\Models\User;

class LoginController
{

    function __construct()
    {

    }

    public function index()
    {
        // session_destroy();
        // exit();
        require "../app/views/login/index.php";
    }

    public function log()
    {
        if(isset($_SESSION["user"]) && !empty(isset($_SESSION["user"]))) {
            //Usuario logeado
            unset($_SESSION["user"]);
            header("Location:/login");

        } else {
            //Usuario no logueado
            header("Location:/login");
        }
    }

    public function login()
    {
        $email = $_REQUEST["email"];
        $password = $_REQUEST["password"];
        $user = User::find_email($email);


        if($user != null && password_verify($password,$user->password)) {

            $_SESSION["user"] = $user;
            unset($_SESSION["message_login"]);

            require "../app/views/user/show.php";
            return;

        } else {
            unset($_SESSION["user"]);
            $_SESSION["message_login"] = "Usuario o contraseña esta mal!";

            header("Location:/login");
            return;
        }


    }
}
