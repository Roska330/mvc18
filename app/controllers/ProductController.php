<?php
namespace App\Controllers;

use \App\Models\Product;
use \App\Models\ProductType;

class ProductController
{

    function __construct(){}

    public function index()
    {
        if(isset($_SESSION["user"]) && !empty(isset($_SESSION["user"]))) {
            //Se ha logeado ya el usuario
            $products = Product::all();
            require "../app/views/product/index.php";

        } else {
            //No se ha logueado el usuario
            header("Location:/login");
        }
    }
    public function create()
    {
        //$catProducts = Category::categories();

        //Aqui le pasare la lista con los nombres de las categoria en un futuro
        $types = ProductType::all();

        require "../app/views/product/register.php";


    }

    public function show($args)
    {
        $id = (int) $args[0];
        $product = Product::find($id);

        require "../app/views/product/show.php";
    }


    public function delete($args)
    {
        // Recogemos el id
        $id = (int) $args[0];
        $product = Product::find($id);
        $product->delete();
        header("Location:/product");
    }


    public function edit($args)
    {
        $id = (int) $args[0];
        $product = Product::find($id);
        $types = ProductType::all();
        require "../app/views/product/edit.php";
    }

    public function update()
    {
        $id = $_REQUEST["id"];
        $product = Product::find($id);
        $product->id = $id;
        $product->name = $_REQUEST["name"];
        $product->price = $_REQUEST["price"];
        $product->type_id = $_REQUEST["type"];

        $product->save();
        header("Location:/product");
    }

    public function productsOfCategory($args)
    {
        $id = (int) $args[0];

        $products = Product::category($id);
        $type = ProductType::find($id);

        require "../app/views/productType/show.php";
    }

}
