<?php
namespace App\Controllers;
use \App\Models\User;

class UserController
{

    function __construct()
    {
    }


    public function index()
    {
        $users = User::paginate(5);
        $rowCount = User::rowCount();

        $pages = ceil($rowCount / 5);
        if(isset($_REQUEST["page"])) {
            $page = (int) $_REQUEST["page"];
        } else {
            $page = 1;
        }

        require "../app/views/user/index.php";
    }

    public function show($args)
    {
        $id = (int) $args[0];
        $user = User::find($id);
        // echo $user;
        // var_dump($user);
        // exit();
        require "../app/views/user/show.php";
    }

    public function create()
    {
        require "../app/views/user/register.php";
    }



    public function delete($args)
    {
        $id = (int) $args[0];
        $user = User::find($id);
        $user->delete();
        header("Location:/user");
    }

    public function edit($args)
    {
        $id = (int) $args[0];
        $user = User::find($id);
        require "../app/views/user/edit.php";
    }

    public function update()
    {
        $id = $_REQUEST["id"];
        $user = User::find($id);
        $user->name = $_REQUEST["name"];
        $user->surname = $_REQUEST["surname"];
        $user->birthdate = $_REQUEST["birthdate"];
        $user->email = $_REQUEST["email"];
        $user->save();
        header("Location:/user");
    }


}
