<?php
namespace App\Controllers;
use \App\Models\User;
use \App\Models\Product;
use \App\Models\Order;
use \App\Models\OrderProducts;

class RegisterController
{
    function __construct(){}

    public function index()
    {
        require "../app/views/register/index.php";
    }

    public function storeUser()
    {
        if(isset($_REQUEST["name"]) && isset($_REQUEST["surname"]) && isset($_REQUEST["email"]) && isset($_REQUEST["birthdate"]) && isset($_REQUEST["password"])) {
            $user = new User();
            $user->name = $_REQUEST["name"];
            $user->surname = $_REQUEST["surname"];
            $user->email = $_REQUEST["email"];
            $user->birthdate = $_REQUEST["birthdate"];
            $user->password = $_REQUEST["password"];
            $user->insert();
            unset($_SESSION["message_user"]);
            header("Location:/user");
        } else {
            $message = "Debes rellenar todos los campos.";
            $_SESSION["message"] = $message;
            require "../app/views/user/register.php";
        }
    }
    
    public function storeProduct()
    {
        if(isset($_REQUEST["name"]) && isset($_REQUEST["price"]) && isset($_REQUEST["type"])) {
            $product = new Product();
            $product->name = $_REQUEST["name"];
            $product->price = $_REQUEST["price"];
            $product->type_id = $_REQUEST["type"];
            $product->insert();
            unset($_SESSION["message_product"]);
            header("Location:/product");
        } else {
            $message = "Debes rellenar todos los campos.";
            $_SESSION["message"] = $message;
            require "../app/views/product/register.php";
        }
    }

    public function storeOrders()
    {
        if(isset($_SESSION["basket"]) && !empty($_SESSION["basket"])) {
            $basket = $_SESSION["basket"];
            $order = new Order();
            $orderProducts = new OrderProducts();
            $price = 0;

            // Precio total del pedido
            foreach ($_SESSION["basket"] as $product) {
                $price += $product->price;
            }
            // Creamos un producto
            $order->date = date('Y/m/d');
            $order->price = $price;
            $order->user_id = $_SESSION["user"]->id;
            $id = $order->insert();


            // var_dump($order::lastID());
            // exit();
            // Agregamos los productos del pedido
            foreach ($_SESSION["basket"] as $product) {
                $orderProducts->product_id = (int)$product->id;
                $orderProducts->order_id = $id;
                $orderProducts->quantity = $product->quantity;
                $orderProducts->price = $product->price/$product->quantity;
                $orderProducts->insert();
            }

            unset($_SESSION["basket"]);
            unset($_SESSION["message"]);
            header("Location:/product");
        } else {
            $message = "No hay productos en la cesta";
            $_SESSION["message"] = $message;
            require "../app/views/order/basket.php";

        }
    }

}
