<?php
namespace App\Models;

use PDO;
use Core\Model;

require_once "../core/Model.php";

class Product extends Model
{

    function __construct()
    {
    }

    public static function all()
    {
        $db = Product::db();

        $statement = $db->query("SELECT * FROM products");
        $products = $statement->fetchAll(PDO::FETCH_CLASS,Product::class);

        return $products;
    }

    public static function find($id){
        $db = Product::db();

        $statement = $db->prepare("SELECT * FROM products WHERE id = :id");
        $statement->execute(array(":id" => $id));
        $statement->setFetchMode(PDO::FETCH_CLASS,Product::class);
        $product = $statement->fetch(PDO::FETCH_CLASS);
        return $product;
    }

    public function insert()
    {
        $db = Product::db();
        $statement = $db->prepare("INSERT INTO products(name,price,type_id) VALUES (:name,:price,:type)");

        $statement->bindValue(":name", $this->name);
        $statement->bindValue(":price", $this->price);
        $statement->bindValue(":type", $this->type_id);

        return $statement->execute();
    }


    public function delete()
    {
        $db = Product::db();
        $statement = $db->prepare("DELETE FROM products WHERE id = :id");

        $statement->bindValue(":id", $this->id);

        return $statement->execute();

    }

    public function save()
    {
        // var_dump($this);
        // exit();
        $db = Product::db();
        $statement = $db->prepare("UPDATE products SET name=:name, price=:price, type_id=:type_id  WHERE id = :id");

        $statement->bindValue(":name", $this->name);
        $statement->bindValue(":price", $this->price);
        $statement->bindValue(":type_id", $this->type_id);
        $statement->bindValue(":id", $this->id);

        return $statement->execute();
    }

    public function category($id)
    {
        $db = Product::db();
        $statement = $db->prepare("SELECT * FROM products WHERE type_id = :id");

        $statement->bindValue(":id", $id);
        $statement->execute();
        $products = $statement->fetchAll(PDO::FETCH_CLASS,Product::class);
        return $products;

    }
    public function type()
    {
        //un producto pertenece a un tipo:
        $db = Product::db();
        $statement = $db->prepare('SELECT * FROM product_types WHERE id = :id');
        $statement->bindValue(':id', $this->type_id);
        $statement->execute();
        $product = $statement->fetchAll(PDO::FETCH_CLASS, ProductType::class)[0];

        return $product;
    }

}
