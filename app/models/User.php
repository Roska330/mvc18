<?php
namespace App\Models;

use PDO;
use Core\Model;

class User extends Model
{

    function __construct()
    {
        $this->dateOld = strtotime($this->birthdate);
        $this->date = new \DateTime($this->birthdate);
    }

    public static function all()
    {
        $db = User::db();

        $statement = $db->query("SELECT * FROM users");
        $users = $statement->fetchAll(PDO::FETCH_CLASS,User::class);

        return $users;
    }

    public function insert()
    {
        $db = User::db();
        $statement = $db->prepare("INSERT INTO users(name,surname,email,birthdate,password) VALUES (:name,:surname,:email,:birthdate,:password)");

        $statement->bindValue(":name", $this->name);
        $statement->bindValue(":surname", $this->surname);
        $statement->bindValue(":email", $this->email);
        $statement->bindValue(":birthdate", $this->birthdate);
        $statement->bindValue(":password", $this->password);

        return $statement->execute();
    }

    public static function find($id){
        $db = User::db();

        $statement = $db->prepare("SELECT * FROM users WHERE id = :id");
        $statement->execute(array(":id" => $id));
        $statement->setFetchMode(PDO::FETCH_CLASS,User::class);
        $user = $statement->fetch(PDO::FETCH_CLASS);
        return $user;
    }

    public static function find_email($email){
        $db = User::db();

        $statement = $db->prepare("SELECT * FROM users WHERE email = :email");
        $statement->execute(array(":email" => $email));
        $statement->setFetchMode(PDO::FETCH_CLASS,User::class);
        $user = $statement->fetch(PDO::FETCH_CLASS);
        return $user;
    }



    public function delete()
    {
       $db = User::db();
        $statement = $db->prepare("DELETE FROM users WHERE id = :id");

        $statement->bindValue(":id", $this->id);

        return $statement->execute();

    }

    public function save()
    {
        $db = User::db();
        $statement = $db->prepare("UPDATE users SET name = :name, surname = :surname, email = :email, birthdate =:birthdate WHERE id = :id");

        $statement->bindValue(":name", $this->name);
        $statement->bindValue(":surname", $this->surname);
        $statement->bindValue(":birthdate", $this->birthdate);
        $statement->bindValue(":email", $this->email);
        $statement->bindValue(":id", $this->id);

        return $statement->execute();
    }

    public function paginate($length = 10)
    {
        if(isset($_REQUEST["page"])) {
            $page = (int) $_REQUEST["page"];
        } else {
            $page = 1;
        }

        $offset = ($page - 1) * $length;

        $db = User::db();
        $statement = $db->prepare("SELECT * FROM users LIMIT :pagesize OFFSET :offset");
        $statement->bindValue(":pagesize", $length,PDO::PARAM_INT);
        $statement->bindValue(":offset", $offset,PDO::PARAM_INT);
        $statement->execute();


        $users = $statement->fetchAll(PDO::FETCH_CLASS,User::class);

        return $users;
    }
    public static function rowCount()
    {
        $db = User::db();

        $statement = $db->prepare("SELECT count(id) as count FROM users");
        $statement->execute();

        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);
        return $rowCount["count"];
    }

    public function setPassword($password)
    {
        $password = password_hash($password,PASSWORD_BCRYPT);
        $db = User::db();
        $statement = $db->prepare("UPDATE users SET password = :password WHERE id = :id");

        $statement->bindValue(":id",$this->id);
        $statement->bindValue(":password",$password);
        return $statement->execute();

    }
}
