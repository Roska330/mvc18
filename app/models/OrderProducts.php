<?php
namespace App\Models;

use PDO;
use Core\Model;


class OrderProducts extends Model
{

    function __construct() {
        //$this->datee = new \DateTime($this->date);;
    }

    public static function all()
    {
        $db = OrderProducts::db();

        $statement = $db->query("SELECT * FROM orders");
        $products = $statement->fetchAll(PDO::FETCH_CLASS,OrderProducts::class);

        return $products;
    }

    public function insert()
    {
        $db = OrderProducts::db();
        var_dump($this);
        // exit();
        $statement = $db->prepare("INSERT INTO orders_products(order_id,price,product_id,quantity) VALUES (:order_id,:price,:product_id,:quantity)");

        $statement->bindValue(":order_id", $this->order_id);
        $statement->bindValue(":price", $this->price);
        $statement->bindValue(":product_id", $this->product_id);
        $statement->bindValue(":quantity", $this->quantity);
        return $statement->execute();
    }


    public function product()
    {
        //un producto pertenece a un tipo:
        $db = Product::db();
        $statement = $db->prepare('SELECT * FROM products WHERE id = :id');
        $statement->bindValue(':id', $this->product_id);
        $statement->execute();
        $product = $statement->fetchAll(PDO::FETCH_CLASS, Product::class)[0];

        return $product;
    }

}
