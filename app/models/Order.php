<?php
namespace App\Models;

use PDO;
use Core\Model;


class Order extends Model
{

    function __construct(){}

    public static function all()
    {
        $db = Order::db();

        $statement = $db->query("SELECT * FROM orders");
        $users = $statement->fetchAll(PDO::FETCH_CLASS,Order::class);

        return $users;
    }

    public function insert()
    {
        $db = Order::db();
        $statement = $db->prepare("INSERT INTO orders(date,price,user_id) VALUES (:date,:price,:user_id)");

        $statement->bindValue(":date", $this->date);
        $statement->bindValue(":price", $this->price);
        $statement->bindValue(":user_id", $this->user_id);
        $statement->execute();

        return $db->lastInsertId();
    }

    public function user()
    {
        //un producto pertenece a un tipo:
        $db = User::db();
        $statement = $db->prepare('SELECT * FROM users WHERE id = :id');
        $statement->bindValue(':id', $this->user_id);
        $statement->execute();
        $user = $statement->fetchAll(PDO::FETCH_CLASS, User::class)[0];

        return $user;
    }

    public static function products($id)
    {

        $db = OrderProducts::db();
        $statement = $db->prepare('SELECT * FROM orders_products WHERE order_id = :id');
        $statement->bindValue(':id', $id);
        $statement->execute();
        $products = $statement->fetchAll(PDO::FETCH_CLASS, OrderProducts::class);

        return $products;
    }
}
