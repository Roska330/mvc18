<?php
namespace App\Models;

use PDO;
use Core\Model;

require_once "../core/Model.php";


class ProductType extends Model
{

    function __construct()
    {

    }

    public static function all()
    {
        $db = ProductType::db();

        $statement = $db->query("SELECT * FROM product_types");
        $products = $statement->fetchAll(PDO::FETCH_CLASS,ProductType::class);

        return $products;
    }

    public static function find($id){
        $db = ProductType::db();

        $statement = $db->prepare("SELECT * FROM product_types WHERE id = :id");
        $statement->execute(array(":id" => $id));
        $statement->setFetchMode(PDO::FETCH_CLASS,ProductType::class);
        $type = $statement->fetch(PDO::FETCH_CLASS);
        return $type;
    }


}
