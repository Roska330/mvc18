# Historia

- Entrega  via Pull Request día 19/nov

- Ejecuta los sql 02 y 03
- Crea un controlador ProductController responsable de un CRUD completo sobre la tabla "products".
    - lista: /product/index
    - detalle: /product/show/{id}
    - formulario de alta: /product/create
    - alta del producto: /product/store   (action del formulario anterior)
    - edición de un producto: /product/edit/{id}
    - actualización del producto: /product/update  (action del formulario anterior)
    - borrado de producto: /product/delete/{id}
