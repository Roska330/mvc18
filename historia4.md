# Historia

- Entrega  via Pull Request día 21/nov

- Modifica el CRUD sobre la tabla de productos:

	- En la lista de productos debe aparecer el nombre del tipo y no su id

	- En el alta y modificación de producto debe aparecer un select con la lista de tipos para elegir el adecuado.

- Además debes crear el controlador ProducttypeController que y los métodos index y show:
	- Añade la ruta /producttype que muestra todos los tipos de productos.

	- Añade la lista /producttype/show/{id} que muestra el detalle de un tipo de producto y, debajo, la lista de productos asocidados disponibles.