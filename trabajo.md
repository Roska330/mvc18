# MVC18: Peña App

Vamos a construir una aplicación para gestionar una peña de amigos.

Cada día debes completar una pequeña _historia de usuario_.
Encontrarás la información necesaria en esta rama del repositorio de clase MVC18.

- Debes partir de un fork de este repositorio (mvc18).
- Añade el repositorio del profesor como remoto:

```
git remote add profe git@bitbucket.org:rafacabeza/mvc18.git
git fetch profe
git checkout --track profe/trabajo
```

- Para descargar código del mismo

```
git fetch profe
```

- Para hacer visible una rama del profesor (trabajo por ejemplo):

```
git checkout --track profe/trabajo
```

- Para actualizarla después

```
git pull
```
